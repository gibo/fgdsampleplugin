//
//  FGDOtherClasses.swift
//  FGDSamplePlugin
//
//  Created by Gilberto De Faveri on 23/11/15.
//  Copyright © 2015 Foreground (omnidea srl). All rights reserved.
//
//  A Swift port with some minor improvements of RMSSamplePlugin, which is part
//  of Realmac Software's official RapidWeaver plugin SDK.
//
//  =============================================================================
//  This sofware is NOT ENDORSED NOR SUPPORTED by Realmac Software in any way.
//  =============================================================================

import Foundation

class FGDSamplePluginContentViewController : NSViewController {
    
    @IBOutlet var htmlView: RWHTMLView?
    var content: String? {
        get {
            return htmlView!.string
        }
    }
    
    required init?(representedObject: AnyObject) {
        
        super.init(nibName: "FGDSamplePluginContentView", bundle: FGDSamplePlugin.bundle())
        self.representedObject = representedObject
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        
        let p = self.representedObject as! FGDSamplePlugin
        htmlView?.string = p.content
        htmlView?.setHasBorder(false)
        
    }
    
    func textDidChange(aNotification: NSNotification) {
        
        let p = self.representedObject as! FGDSamplePlugin
        p.broadcastPluginChanged()
        
    }
    
}